package ai.ady.afreeca.domain.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class AdProductRepositoryTest {

    @Autowired
    private AdProductRepository repository;

    @Test
    public void test(){
        log.info("{}", repository.findAll());
    }
}
