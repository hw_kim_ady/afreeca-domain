package ai.ady.afreeca.domain.entity;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "ad_products")
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@Getter
@EqualsAndHashCode(of = "id", callSuper = false)
@ToString(callSuper = true)
public class AdProduct extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Boolean status;

    private Boolean deleted;

    @Column(length = 30)
    private String adProductName;

    private String salesChannel;

    private ZonedDateTime sellingPeriodStart;

    private ZonedDateTime sellingPeriodEnd;

    @Column(length = 10)
    @Enumerated(EnumType.STRING)
    private BuyingType buyingType;

    @Column(length = 5)
    @Enumerated(EnumType.STRING)
    private BidStrategy bidStrategy;

    private Long minBidPrice;

    @Column(length = 200)
    private String adProductIdentityImage;

    @Column(length = 200)
    private String adProductIdentityTest;

    private Boolean targetingSupportingStatus;

    private String availableTargetings;

    private String creativeType;

    private String creativeSize;

    private String fileFormat;

    private String maxFileSize;

    @Column(length = 10)
    @Enumerated(EnumType.STRING)
    private SkipOption skipOption;

    private Integer skipButtonExposureTime;

    public enum BuyingType {
        FIXED, AUCTION
    }

    public enum BidStrategy {
        CPV, CPC, CPM
    }

    public enum SkipOption {
        SKIP, NONSKIP
    }
}
