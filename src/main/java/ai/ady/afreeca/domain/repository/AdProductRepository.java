package ai.ady.afreeca.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ai.ady.afreeca.domain.entity.AdProduct;

@Repository
public interface AdProductRepository extends JpaRepository<AdProduct, Integer> {
}
